package testtest.Service;

import org.springframework.stereotype.Repository;
import testtest.Entity.bids;
import testtest.Entity.item;
import testtest.Entity.users;
import org.springframework.beans.factory.annotation.Autowired;
import testtest.Repository.usersSql;
import testtest.Repository.bidsSql;
import testtest.Repository.itemSql;

import java.util.List;
import java.util.Map;


@Repository
public class TestService {

    @Autowired
    usersSql userQuery;

    @Autowired
    bidsSql bidQuery;

    @Autowired
    itemSql itemQuery;

    //@Override
    public List<users> getUsers() {
        return userQuery.getByUsers();
    }

    //1
    public List<bids> getBidUser(int id){

        return bidQuery.getBidsUser(id);
    }
    //2
    public List<item> getItemUser(int id){

        return itemQuery.getItemUser(id);

    }

    //3
    public List<item> getItemLikeT(String  like){

        return itemQuery.getItemLikeTittle(like);

    }

    //4 query
    public List<item> getItemLikeD(String  like){

        return itemQuery.getItemLikeDescription(like);

    }

    // 5 query
    public List<Map<String, Object>> getItemA(){

        return itemQuery.getItemAvg();

    }

    // 6 query
    public List<Map<String, Object>> getBidMax(){

        return bidQuery.getBidmMax();

    }

    // 7 query
    public List<item> getItemUD(int id){

        return itemQuery.getItemUserDate(id);

    }

    // 8 query
    public void addU(users user){

        userQuery.addUser(user);

    }

    // 9 query
    public void addI(item item){

        itemQuery.addItem(item);

    }

    // 10 query
    public void deleteB(int id){

        bidQuery.deleteBids(id);

    }
    // 11 query
    public void deleteI(int id){

        itemQuery.deleteItems(id);

    }

    // 12 query
    public void multiplicationI(int id){

        itemQuery.multiplicationItems(id);

    }



}
