package testtest.Entity;

import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import java.time.LocalDate;

@Repository
public class item {

    private int item_id;
    private String tittle;
    private String description;
    private double start_price;
    private double bid_increment;
    private LocalDate start_date;
    private LocalDate stop_date;
    private int users_user_id;

    public item() { }

    public item(int item_id, String tittle, String description, double start_price, double bid_increment, LocalDate start_date, LocalDate stop_date, int users_user_id) {
        this.item_id = item_id;
        this.tittle = tittle;
        this.description = description;
        this.start_price = start_price;
        this.bid_increment = bid_increment;
        this.start_date = start_date;
        this.stop_date = stop_date;
        this.users_user_id = users_user_id;
    }

    public item(String tittle, String description, double start_price, double bid_increment, LocalDate start_date, LocalDate stop_date, int users_user_id) {
        this.tittle = tittle;
        this.description = description;
        this.start_price = start_price;
        this.bid_increment = bid_increment;
        this.start_date = start_date;
        this.stop_date = stop_date;
        this.users_user_id = users_user_id;
    }

    public int getItem_id() {
        return item_id;
    }

    public String getTittle() {
        return tittle;
    }

    public String getDescription() {
        return description;
    }

    public double getStart_price() {
        return start_price;
    }

    public double getBid_increment() {
        return bid_increment;
    }

    public LocalDate getStart_date() {
        return start_date;
    }

    public LocalDate getStop_date() {
        return stop_date;
    }

    public int getUsers_user_id() {
        return users_user_id;
    }

    @Override
    public String toString() {
        return "item{" +
                "item_id=" + item_id +
                ", title='" + tittle + '\'' +
                ", description='" + description + '\'' +
                ", start_price=" + start_price +
                ", bid_increment=" + bid_increment +
                ", start_date=" + start_date +
                ", stop_date=" + stop_date +
                ", users_user_id=" + users_user_id +
                '}';
    }
}
