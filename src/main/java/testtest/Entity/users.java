package testtest.Entity;

import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

@Repository
public class users {
    int users_id;
    String full_name;
    String billing_address;
    String login;
    String password;

    public users(){

    }

    public users( int users_id, String full_name, String billing_address, String login, String password) {

        this.users_id = users_id;
        this.full_name = full_name;
        this.billing_address = billing_address;
        this.login = login;
        this.password = password;
    }

    public users(String full_name, String billing_address, String login, String password) {

        this.users_id = users_id;
        this.full_name = full_name;
        this.billing_address = billing_address;
        this.login = login;
        this.password = password;
    }

    public int getUsers_id(int id) {
        return users_id;
    }

    public void setUsers_id(int users_id) {
        this.users_id = users_id;
    }

    public String getFull_name() {
        return full_name;
    }

    public void setFull_name(String full_name) {
        this.full_name = full_name;
    }

    public String getBilling_address() {
        return billing_address;
    }

    public void setBilling_address(String billing_address) {
        this.billing_address = billing_address;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    @Override
    public String toString() {
        return "users{" +
                "users_id=" + users_id +
                ", full_name='" + full_name + '\'' +
                ", billing_address='" + billing_address + '\'' +
                ", login='" + login + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
