package testtest.Entity;

import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import java.time.LocalDate;

@Repository
public class bids {

    private int bid_id;
    private LocalDate bid_date;
    private double bid_value;
    private int items_item_id;
    private int users_user_id;

    public bids(){

    }

    public bids(int bid_id, LocalDate bid_date, double bid_value, int items_item_id, int users_user_id){

        this.bid_id = bid_id;
        this.bid_date = bid_date;
        this.bid_value = bid_value;
        this.items_item_id = items_item_id;
        this.users_user_id = users_user_id;
    }

    @Override
    public String toString() {
        return "bids{" +
                "bid_id=" + bid_id +
                ", bid_date=" + bid_date +
                ", bid_value=" + bid_value +
                ", items_item_id=" + items_item_id +
                ", users_user_id=" + users_user_id +
                '}';
    }
}
