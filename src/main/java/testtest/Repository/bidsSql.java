package testtest.Repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import testtest.Entity.bids;
import testtest.Mapper.bidRowMapper;

import java.util.List;
import java.util.Map;

@Repository
public class bidsSql {

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Autowired
    bidRowMapper Row;



    //@Override
    public List<bids> getBidsUser(int id) {
        String sql = "SELECT * FROM bids WHERE users_user_id = ?";
        return  jdbcTemplate.query(sql, new Object[]{id}, Row::mapRow);
    }

    //6 query
    public List<Map<String, Object>> getBidmMax() {
        String sql = "SELECT max(bid_value), items.tittle FROM bids,items Where items.item_id = bids.items_item_id group by items.tittle";
        return  jdbcTemplate.queryForList(sql);
    }

    //10 query
    public void deleteBids(int id) {
        String sql = "DELETE FROM `bids` WHERE `users_user_id`=?";
        jdbcTemplate.update(sql, id);
    }

}
