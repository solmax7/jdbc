package testtest.Repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import testtest.Entity.bids;
import testtest.Entity.item;
import testtest.Mapper.bidRowMapper;
import testtest.Mapper.itemRowMapper;

import java.util.List;
import java.util.Map;

@Repository
public class itemSql {

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Autowired
    itemRowMapper Row;

    //@Override
    public List<item> getItemUser(int id) {
        String sql = "SELECT * FROM items WHERE users_user_id =?";
        return  jdbcTemplate.query(sql, new Object[]{id}, Row::mapRow);
    }

    //3 query
    public List<item> getItemLikeTittle(String like) {
        String sql = "SELECT * FROM items WHERE tittle LIKE ?";
        like = "%" + like + "%";
        return  jdbcTemplate.query(sql, new Object[]{like}, Row::mapRow);
    }

    //4 query
    public List<item> getItemLikeDescription(String like) {
        String sql = "SELECT * FROM items WHERE description LIKE ?";
        like = "%" + like + "%";
        return  jdbcTemplate.query(sql, new Object[]{like}, Row::mapRow);
    }

    //5 query
    public List<Map<String, Object>> getItemAvg() {
        String sql = "SELECT avg(start_price), users.full_name FROM items,users Where users.users_id = items.users_user_id group by users.full_name";
                return  jdbcTemplate.queryForList(sql);
    }

    //7 query
    public List<item> getItemUserDate(int id) {
        String sql = "SELECT * FROM items Where users_user_id = ? and stop_date >= CURRENT_DATE()";
        return  jdbcTemplate.query(sql, new Object[]{id}, Row::mapRow);
    }

    //9 query
    public void addItem(item item) {
        String sql = "INSERT INTO `items` (`tittle`, `description`, `start_price`, `bid_increment`,`start_date`,`stop_date`,`by_it_now`,`users_user_id`) VALUES (?,?,?,?,?,?,?)";
        jdbcTemplate.update(sql, item.getTittle(), item.getDescription(), item.getStart_price(), item.getBid_increment(),item.getStart_date(), item.getStop_date(), item.getUsers_user_id());
    }

    //11 query
    public void deleteItems(int id) {
        String sql = "DELETE FROM `items` WHERE `users_user_id`=?";
        jdbcTemplate.update(sql, id);
    }

    //12 query
    public void multiplicationItems(int id) {
        String sql = "UPDATE items SET start_price = (start_price * 2)  WHERE users_user_id=?";
        jdbcTemplate.update(sql, id);
    }




}
