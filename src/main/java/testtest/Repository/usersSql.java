package testtest.Repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import testtest.Entity.users;
import testtest.Mapper.usersRowMapper;

import java.util.List;

@Repository
public class usersSql {

        @Autowired
        JdbcTemplate jdbcTemplate;

        @Autowired

        usersRowMapper Row;

        //@Override
        public List<users> getByUsers() {
            String sql = "SELECT * FROM users";
            return  jdbcTemplate.query(sql, Row::mapRow);
        }

        public void addUser(users user) {
            String sql = "INSERT INTO users (full_name, billing_adress, login, password) " +
                    "VALUES (?, ?, ?, ?)";
            jdbcTemplate.update(sql, user.getFull_name(), user.getBilling_address(), user.getLogin(), user.getPassword());
        }

}



