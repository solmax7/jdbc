package testtest.Mapper;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import testtest.Entity.bids;

import org.springframework.jdbc.core.RowMapper;
//import javax.swing.tree.RowMapper;
import javax.swing.tree.TreePath;
import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class bidRowMapper {

    public bids mapRow(ResultSet resultSet, int i) throws SQLException {
        bids bids = new bids(resultSet.getInt("bid_id"),
                resultSet.getDate("bid_date").toLocalDate(),
                resultSet.getDouble("bid_value"),
                resultSet.getInt("items_item_id"),
                resultSet.getInt("users_user_id"));
        return bids;
    }

}
