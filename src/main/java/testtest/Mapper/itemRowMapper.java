package testtest.Mapper;

import org.springframework.stereotype.Component;
import testtest.Entity.item;

import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class itemRowMapper {

    public item mapRow(ResultSet resultSet, int i) throws SQLException {
        item item = new item(resultSet.getInt("item_id"),
                resultSet.getString("tittle"),
                resultSet.getString("description"),
                resultSet.getDouble("start_price"),
                resultSet.getDouble("bid_increment"),
                resultSet.getDate("start_date").toLocalDate(),
                resultSet.getDate("stop_date").toLocalDate(),
                resultSet.getInt("users_user_id"));
        return item;
    }

}
