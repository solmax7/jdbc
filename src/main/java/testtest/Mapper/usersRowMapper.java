package testtest.Mapper;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import testtest.Entity.users;

import org.springframework.jdbc.core.RowMapper;
//import javax.swing.tree.RowMapper;
import javax.swing.tree.TreePath;
import java.sql.ResultSet;
import java.sql.SQLException;


@Component
public class usersRowMapper implements RowMapper {


    public users mapRow(ResultSet resultSet, int i) throws SQLException {
        users users = new users(resultSet.getInt("users_id"),
                resultSet.getString("full_name"), resultSet.getString("billing_address"), resultSet.getString("login"), resultSet.getString("password"));
        return users;
    }


}
