import jdk.internal.util.xml.impl.Parser;
import testtest.Config.AppConfig;
import testtest.Entity.item;
import testtest.Entity.users;
import testtest.Service.TestService;


import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.time.LocalDate;

public class Application {


    public static void main(String[] args) {

        ApplicationContext context =
                new AnnotationConfigApplicationContext(AppConfig.class);

        TestService service = context.getBean(TestService.class);

        System.out.println();

        //1 query
        System.out.println("1 запрос");
       service.getBidUser(2).forEach(System.out::println);
        //
        System.out.println("2 запрос");
        //2 query
        service.getItemUser(2).forEach(System.out::println);//

        System.out.println("3 запрос");
        //3 query
        service.getItemLikeT("Ноутбук").forEach(System.out::println);
        //
        System.out.println("4 запрос");
        //4 query
        service.getItemLikeD("Овощерезка").forEach(System.out::println);
        //
        System.out.println("5 запрос");
        //5 query
        service.getItemA().forEach(System.out::println);
        //
        System.out.println("6 запрос");
        //6 query
        service.getBidMax().forEach(System.out::println);
        //
        System.out.println("7 запрос");
        //7 query
        service.getItemUD(1).forEach(System.out::println);
        //
        System.out.println("8 запрос");
        //8 query
        service.addU(new users("Smoke", "California", "SM","Parol1"));
        //
        System.out.println("9 запрос");
        //9 query
        service.addI(new item("Диван", "Диван раскладной", 150,15, LocalDate.of(2018,03,01),LocalDate.of(2018,05,01),5));
        //
        System.out.println("10 запрос");
        //10 query
        service.deleteB(4);
        //
        System.out.println("11 запрос");
        //11 query
        service.deleteI(2);
        //
        System.out.println("12 запрос");
        //11 query
        service.multiplicationI(1);
        //
    }
}
